<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// route user authentication w/ login & register calls to UserController
Route::post('login', 'UserController@login')->name('login'); //passport API route for auth login
Route::post('register', 'UserController@register'); //passport API route for auth register

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::middleware('auth:api')->group(function (){
     Route::get('user', 'UserController@details');
  });


// route product api resource call to @index & @show - no auth required - no middleware auth:api
Route::apiResource('products', 'ProductController')->except(['store', 'update', 'destroy']);

// route basket api resource call to @store, @show, @destory - auth required 
Route::apiResource('baskets', 'BasketController')->except(['index', 'update'])->middleware('auth:api'); 
// route additional basket functions outside of apiresource - auth required 
Route::post('/baskets/{basket}', 'BasketController@addItem')->middleware('auth:api'); 
Route::delete('/baskets/{basket}', 'BasketController@removeItem')->middleware('auth:api'); 
Route::post('/baskets/{basket}/checkout', 'BasketController@checkout')->middleware('auth:api'); 

// route targeted data api resource call to @index  - auth required 
Route::apiResource('targeted-data-sortedbyoccurance', 'TargetedDataController')->except(['store', 'show', 'update', 'destroy'])->middleware('auth:api');
Route::get('targeted-data-unsorted', 'TargetedDataController@getall')->middleware('auth:api');

// route order api resource call to @index  - auth required 
Route::apiResource('orders', 'OrderController')->except([ 'store', 'show', 'update', 'destroy'])->middleware('auth:api'); // only allow index & show 

// route error function to handle api endpoint error 
Route::any('errors', 'BasketController@errors');
