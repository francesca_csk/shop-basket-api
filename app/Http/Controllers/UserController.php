<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// import necessary library & model
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;


class UserController extends Controller
{

   /**
     * Register/Create new User account
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        // validate the requested data needed for registering user 
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:6', 
            'confirm_password' => 'required|string|same:password', 
        ]);

        // if validation fails - error msg
        if ($validator->fails()) 
        {
            $response = [
                'data' => 'Validation Error.',
                'msg' => $validator->errors()
            ];
            // return error response HTTP status code 404
            return response()->json($response, 404);
        }

        // register new User with creating a new record of user in users tbl
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
 
        // assign user with access token
        $token = $user->createToken('Developer Personal Access Token')->accessToken;
        //return token info to user
        return response()->json(['token' => $token], 200);
    }

    /**
     * Login user & create token for user
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
         // validate data request to login user
         $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        // if validation fails - error msg
        if ($validator->fails()) 
        {
            $response = [
                'data' => 'Validation Error.',
                'msg' => $validator->errors()
            ];
            // return error response HTTP status code 404
            return response()->json($response, 404);
        }

        // get incoming requested credentials from user
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        // authenticate user with input credentials 
        if (auth()->attempt($credentials)) 
        {
            // if user credentials authenticated - authorize access token
            $token = auth()->user()->createToken('Developer Personal Access Token')->accessToken;
            // return token info
            return response()->json([
                'token' => $token
            ], 200);
        } 
        else // user credentials not valid - error response 
        {
            return response()->json([
                'error' => 'Unable to authenticate. '
            ], 401);
        }
    }

    /**
     * Returns Authenticated User Details
     *
     * @return \Illuminate\Http\JsonResponse
     */
     public function details()
     {
         // returns user auth details - for authenticated users only 
         return response()->json([
             'user' => auth()->user()
            ], 200);
     }

       
    /**
     * User Logout - to revoke user access token - only used for testing purposes 
     *
     * @return message
     */
   /* public function logout(Request $request)
    {
        // get user request to revoke token
        $request->user()->token()->revoke();

        return response()->json([
            'msg' => 'You have been successfully logged out.'
        ]);
    } */
  
}
