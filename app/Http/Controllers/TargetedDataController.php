<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// import needed resources & models
use Auth;
use App\TargetedData;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Resources\TargetedDataResource as TargetedDataResource;
use App\Http\Resources\TargetedDataCollection as TargetedDataCollection;

class TargetedDataController extends Controller
{

     /**
     * Display a listing of the resource - for Targeted Data 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // recheck user auth for security purposes before displaying sensitive data
        if (auth('api')->user()->getKey()) 
        {

            // db raw query to filter data by highest occurance 
            // resulting in grouping product_id by highest num occured in db table
            $result = DB::table('targeted_data')
                ->select(DB::raw('count(*) as count, product_id'))
                ->groupBy('product_id')
                ->orderBy('count', 'desc')
                ->get();

            // send result to TargetedDataCollection to output collection as specified
            return new TargetedDataCollection($result);

        }
    }

    /**
     * Display unordered list of the resource - for Targeted Data 
     *
     * @return \Illuminate\Http\Response
     */
    
    public function getAll()
    {
        return new TargetedDataCollection(TargetedData::all());
    }

    // For future milestones - can add more functionailty and queries for sales team use
}
