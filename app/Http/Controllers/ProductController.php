<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// import necessary resources & models
use App\Basket;
use App\Product;
use App\Http\Resources\ProductResource as ProductResource;
use App\Http\Resources\ProductCollection as ProductCollection;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource. 
     * List products 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // returning collection of resources 
      return new ProductCollection(Product::all());
    }

     /**
     * Display the specified resource.
     * Get individual product info - used for testing purposes only* not needed with sample data 
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        // returning single model of transformed data with resource class
        return new ProductResource($product);
    }

    

}
