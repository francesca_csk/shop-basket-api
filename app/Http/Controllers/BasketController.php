<?php

namespace App\Http\Controllers;

// import Models needed 
use App\Basket;
use App\Product;
use App\User;
use App\ItemBasket;
use App\RemovedItem;
use App\Order;
use App\TargetedData;

// import Collection needed
use App\Http\Resources\ItemBasketCollection as ItemBasketCollection;

// import in Validator to validate incoming data requests
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;

class BasketController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // get user auth 
       $user_id = auth('api')->user()->getKey();

        // new basket request 
        $basket = Basket::create([
            // create unique id for new basket 
            'id' => md5(uniqid(rand(), true)),
            // create unique key for new basket 
            'key' => md5(uniqid(rand(), true)),
            //check if user_id set
            // if user_id would not be mandatory could set it to isset($user_id) ? $user_id : null,
            'user_id' => $user_id,
        ]);

        // response msg with basket token and key 
        return response()->json([

            'success' => true,

            '_token' => $basket->id, // token for basket

            '_key' => $basket->key, // key for basket

            // reponse msg to output basket id & basket key
            'msg' => 'A new shopping basket has been created.',

        ], 201);

    }

    /**
     * Display the specified resource.
     * - Show Items in Basket
     * @param  \App\Basket  $basket
     * @return \Illuminate\Http\Response
     */
    public function show(Basket $basket, Request $request)
    {
        // validate incoming requests - basket key 
        $validator = Validator::make($request->all(), [
             // pass in basket key to validate param
            '_key' => 'required',
        ]);

        // if basket key validation fails - return validation error msg 
        if ($validator->fails()) 
        {
            // validation error response - 400
            return response()->json([
                'errors' => $validator->errors(),
            ], 400);
        }

        // get user auth
       // $user_id = auth('api')->user()->getKey();

        // retrieve input request
        $_key = $request->input('_key');

        // (inner if cond) check that request for basket key matches current key for basket
        if ($basket->key == $_key) 
        {
            // json response with items in current basket
            return response()->json([
                'basket' => $basket->id,
                'Products in Basket' => new ItemBasketCollection($basket->basketitems),  
            ], 200);
        } 
        else // basket key does not match (part of inner if/else cond)
        {
            // error msg - invalid baskey key param provided 
            return response()->json([
                'msg' => 'Provided invalid basket key.',
            ], 400);
        }
 
    }

    /**
     * Add Item to basket
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Basket  $basket
     * @return \Illuminate\Http\Response
     */
    public function addItem(Request $request, Basket $basket)
    {
        // validate incoming requests - basket key & product id
        $validator = Validator::make($request->all(), [
            '_key' => 'required',
            'product_id' => 'required',
             'qty' => 'required|numeric|min:1|max:3', // quantity 1 <= x >= 3
        ]);

         // if basket key & product id validation fails - return error msg - HTTP status code 400
        if ($validator->fails()) 
        {
            return response()->json([
                'errors' => $validator->errors(),
            ], 400);
        }

         // get user auth
        $user_id = auth('api')->user()->getKey();
         
        // retrieve baskey key and item info input requests 
        $_key = $request->input('_key');
        $product_id = $request->input('product_id');
        $qty = $request->input('qty');

        // check that request for basket key matches current key for basket
        if ($basket->key == $_key) 
        {
            // exception to check that product exist to avoid proceeding with invalid product in basket
           try { 
                    // check that product is found if not catch ModelNotFoundException - to respond w/ own error msg
                $checkProduct = Product::findOrFail($product_id);
            } catch (ModelNotFoundException $exception) 
            {
                return response()->json([
                    'msg' => 'Invalid Product.',
                ], 404);
            }

            // check if product already in basket
            // if product already in basket - update product quantity 
            $itemInbasket = ItemBasket::where([
                    // retrieve record of item if already existance in basket 
                    'basket_id' => $basket->getKey(), 'product_id' => $product_id])->first(); 

            // if product not in basket yet - continue with adding it to basket
            if(!$itemInbasket)
            {
                // add product to shopping basket - to store in item_baskets tbl 
                ItemBasket::create([
                        'basket_id' => $basket->getKey(), 
                        'product_id' => $product_id,
                        'qty' => $qty,
                        //'user_id' => $user_id,
                    ] );

                return response()->json([
                    'msg' => 'Product was successfully added to basket.'
                ], 200);
            }
            else // product already in basket - update quantity of product in basket
            {
                // get record of item quantity (to update qty with)
                $itemInbasket->qty = $qty;

                    // retrieve current record of existing basket item to update item qty in basket
                ItemBasket::where([
                    'basket_id' => $basket->getKey(), 
                    'product_id' => $product_id,
                    //'user_id' => $user_id, 
                    ]) ->update(['qty' => $qty]); // update item qty in item_baskets tbl
                    
                return response()->json([
                    'msg' => 'Quantity of product was successfully updated in shopping basket.'
                ], 200);
            }
        } 
        else  // basket key param does not match current basket 
        {
            // error msg - invalid baskey key param provided 
            return response()->json([
                'msg' => 'Provided invalid basket key.',
            ], 400);
        }
   
    }

     /**
     * Remove an item from basket
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Basket  $basket
     * @return \Illuminate\Http\Response
     */
    public function removeItem(Request $request, Basket $basket)
    {
        // validate incoming requests for basket key & product id
        $validator = Validator::make($request->all(), [
            '_key' => 'required',
            'product_id' => 'required',
        ]);

        // if validation fails for basket key & product id - return validation error msg 
        if ($validator->fails()) 
        {
            return response()->json([
                'errors' => $validator->errors(),
            ], 400);
        }

        // get user auth
        $user_id = auth('api')->user()->getKey();

        // retrieve input requests 
        $_key = $request->input('_key');
        $product_id = $request->input('product_id');
    
        // (inner if cond) check that request for basket key matches basket
        if ($basket->key == $_key) 
        {
            // retrieve basket item from item_baskets tbl
            $remitem = ItemBasket::where('product_id', $product_id);

            // find item info from products tbl with its primary key product_id 
            $product = Product::findOrFail($product_id);

            // create a record of item removed from basket
            RemovedItem::create([
                'basket_id' => $basket->getKey(), 
                // get product id from tbl products row
                'product_id' => $product_id,
                // get product name from product row 
                'product_name' => $product->name,
                // get product price from product row 
                'product_price' => $product->price,
                'user_id' => $user_id,
                'removed_at' => time(),
                ] );
        
            // officially remove item from basket 
            $remitem->delete();

            return response()->json([
                "msg" => "Item removed from shopping basket."
            ], 202);
        } 
        else // basket key does not match basket (part of inner if/else cond)
        {
            // error msg - invalid baskey key provided 
            return response()->json([
                'msg' => 'Provided invalid basket key.',
            ], 400);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *   - Remove specified Basket from storage
     * @param  \App\Basket  $basket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Basket $basket)
    {
        // validate incoming requests - basket key & product id
        $validator = Validator::make($request->all(), [
            '_key' => 'required',
        ]);

        // if basket key validation fails - return validation error msg 
        if ($validator->fails()) 
        {
             // validation error response - 400
            return response()->json([
                'errors' => $validator->errors(),
            ], 400);
        }

        // retrieve input request _key
        $_key = $request->input('_key');

        // check that request for basket key matches current basket 
        if ($basket->key == $_key) 
        {
            // delete baskey key
            $basket->delete();
            return response()->json(null, 204);
        } 
        else // basket key does not match basket - send json msg error
        {
            // error msg - invalid basket key provided 
            return response()->json([
                'msg' => 'Provided invalid basket key.',
            ], 400);
        }

    }

    /**
     *  Checkout basket with items in basket.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\basket  $basket
     * @return \Illuminate\Http\Response
     */
    public function checkout(Basket $basket, Request $request)
    {
        // validate incoming requests - to make sure all necessary data is provided
        $validator = Validator::make($request->all(), [
            '_key' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'street_1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'country' => 'required',
            'email' => 'required',
        ]);

       // if validation fails - return validation error msg 
       if ($validator->fails()) 
       {
            // validation error response - 400
            return response()->json([
                'errors' => $validator->errors(),
            ], 400);
        }

        // get user auth
        $user_id = auth('api')->user()->getKey();

        // see if user removed any basket items before proceeding with checkout process
        // query removed_items table to get any collection of items that match user_id && basket_id 
       $remitems = RemovedItem::where('user_id', $user_id)
                                ->where('basket_id', $basket->getKey())
                                ->get();

        // if removed_items tbl retrieved items that match both user_id && basket_id
        if ($remitems) 
        {
            // loop/iterate through retrieved collection of item/s 
            foreach ($remitems as $remitem) 
           {
               // record targeted data of removed item/s from basket before checkout 
               $targetd = TargetedData::create([
                    // get removed item/s info to process into targeted_data tbl
                    'product_id' => $remitem->product_id,
                    'product_name' => $remitem->product_name,
                    'product_price' => $remitem->product_price,
                    'removed_at' => $remitem->removed_at,
                    'user_id' => $user_id,
                    ]);
            } // end foreach loop - after recording the necessary removed items
        } // end if cond to check/process user removed items

        // now continue with rest of checkout process

        // retrieve input request for _key
        $_key = $request->input('_key');

        // check that request for basket key matches current basket
        if ($basket->key == $_key) 
        {
            // retrieve rest of input request params to process checkout and create order with
            $first_name = $request->input('first_name');
            $last_name = $request->input('last_name');
            $street_1 = $request->input('street_1');
            $city = $request->input('city');
            $state = $request->input('state');
            $zip = $request->input('zip');
            $country = $request->input('country');
            $email = $request->input('email'); 

           // prepare total price for order 
            $items_total= (float) 0.0;

            $basketitems = $basket->basketitems;

            // loop/iterate through basket item/s 
            foreach ($basketitems as $item) 
            {
                // retrieve basket item info from products table by finding it's primary key product_id
                $product = Product::find($item->product_id);
                // get product price from retrieved item record 
                $price = $product->price;
                // add up total price for basket item/s
                $items_total = $items_total + ($price * $item->qty);
                // save retrieved product record  
                $product->save();
            } // end foreach loop to iterate through basket item/s

            // create unique transaction_id 
            $transaction_id = md5(uniqid(rand(), true));

            // Create Order with processing incoming requested info into order table
            $order = Order::create([
                'products' => json_encode(new ItemBasketCollection($basketitems)),
                'items_total' => $items_total,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'street_1' => $street_1,
                'city' => $city,
                'state' => $state,
                'zip' => $zip,
                'country' => $country,
                'email' => $email,
                'user_id' => $user_id,
                'transaction_id' => $transaction_id,
                //'status' => $status, (ex: "Awaiting Payment)
            ]);

            // delete baskey key 
            $basket->delete();
            
            // return success json response with order_id
            return response()->json([
                'order_id' => $order->getKey(),
                'msg' => 'Order submitted.',
            ], 200);
        } 
        else // basket key does not match basket - send json msg error
        {
            // error msg - invalid basket key provided 
            return response()->json([
                'msg' => 'Provided invalid basket key.',
            ], 400);
        }
    }

    // function to provide error response for invalid API endpoints 
    public function errors() 
    {
        // respond with error msg -  HTTP status code 501 - not implemented 
        return response()->json(['msg' => 'Error msg.'], 501);
    }
} 
