<?php

namespace App\Http\Controllers;

// import necessary resources & model
use App\Http\Resources\OrderResource as OrderResource; // add Order Resource class
use App\Http\Resources\OrderCollection as OrderCollection; // add Order Collection
use App\Order;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the User orders.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // if auth - get user order history from orders tbl
       $orders = Order::where('user_id', auth()->id())->get();
       // return collection of orders
       return new OrderCollection($orders);
    }

}
