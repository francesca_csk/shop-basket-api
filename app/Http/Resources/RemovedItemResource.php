<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RemovedItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            // unofficial data 
            'product_name' => $this->product_name,
            'product_id' => $this->product_id,
            'product_price' => $this->product_price,
            'removed_at' => (string)$this->removed_at,
            'basket_id' => $this->basket_id,
        ];
    }
}
