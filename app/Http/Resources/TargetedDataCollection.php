<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TargetedDataCollection extends ResourceCollection
{
    public $collects = 'App\Http\Resources\TargetedDataResource';
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'Basket Items removed before checkout:' => $this->collection,
        ];
    }
}
