<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ItemBasketCollection extends ResourceCollection
{
    public $collects = 'App\Http\Resources\ItemBasketResource';
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection;
        //return parent::toArray($request);
    }
}
