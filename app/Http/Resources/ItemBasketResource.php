<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Product;

class ItemBasketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       // return parent::toArray($request);
       $product = Product::find($this->product_id);
     
        return [

            'product_id' => $this->product_id,
            'name' => $product->name,
            'price' => $product->price,
             'qty' => $this->qty,

             // added these items for future milestones of collecting user data
             // such as top most popular items sold & least popular items
             'created_at' => (string)$this->created_at,
             'updated_at' => (string)$this->updated_at,
        ];
    }
}
