<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RemovedItemCollection extends ResourceCollection
{
    public $collects = 'App\Http\Resources\RemovedItemResource';
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'Unofficial Products removed from basket (not official data - as it could be part of left behind baskets)' => $this->collection,
            
        ];
    }
}
