<?php

namespace App\Http\Resources;

use App\Http\Resources\ItemBasketCollection as ItemBasketCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'products' => json_decode($this->products),
            'items_total' => $this->items_total,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'billing_addresss' => $this->street_1,
            'billing_city' => $this->city,
            'billing_state' => $this->state,
            'billing_zip' => $this->zip,
            'billing_country' => $this->country,
            'email' => $this->email,
            'transaction_id' => $this->transaction_id,
        ];
    }
}
