<?php

namespace App\Http\Resources;

use App\Product;
use App\RemovedItem;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\JsonResource;

class TargetedDataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $product = Product::findOrfail($this->product_id);
        $item = RemovedItem::findOrfail($this->product_id);

        return [
            // *targeted discounts*  reference for data items removed from basket before checkout 
            'product_id' => $this->product_id,
            'product_name' => $product->name,
            'product_price' => $product->price,

            // only data below for unfiltered data (lists every single item
             'last_removed_at' => (string)$item->removed_at,
            //'user_id' => $this->user_id,
        ];
    }

}
