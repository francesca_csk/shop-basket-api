<?php

namespace App\Http\Middleware;

use Closure;
// to include logging class
use Illuminate\Support\Facades\Log;

class Logging
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // log the HTTP method for requests in storage/logs
        Log::debug($request->method());
        return $next($request);
    }

    public function terminate($request, $response)
    {
        // log responses in storage/logs
        Log::debug($response->status());
    }
}
