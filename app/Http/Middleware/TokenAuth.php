<?php

namespace App\Http\Middleware;

use Closure;

// for testing use only 
class TokenAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // USED FOR TESTING PURPOSES ONLY 
        // header type for token auth 'TEST-API-TOKEN'
       /* $token = $request->header('AUTHENTICATION');

        // token authentication method w/ specified token below 'token-value-test'

        // for testing use only - specify in Kernel.php 
        // api =>  \App\Http\Middleware\TokenAuth::class,
        if ('bearer-token' != $token)
        {
            abort(401, 'Auth Token not found');
        }

        return $next($request);*/
    }
}
