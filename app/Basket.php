<?php

namespace App;

use App\ItemBasket;
use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    // Basket model $fillable field for permitted assignments to baskets tbl
    protected $fillable = ['id','key', 'content', 'user_id']; 

    // do not increment primary id because consists of special string
    public $incrementing = false;

    public function basketitems()
    {
        return $this->hasMany('App\ItemBasket', 'basket_id');
    }

}
