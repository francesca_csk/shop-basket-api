<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemBasket extends Model
{
    // ItemBasket model $fillable field for permitted assignments to item_baskets tbl
    protected $fillable = [ 'basket_id', 'product_id', 'qty', 'user_id'];

    // relationship - basket item belong to Basket 
    public function basket()
    {
        return $this->belongsTo(Basket::class);
    }

    // relationship - basket item can only belong to one product 
    public function product()
    {
        return $this->hasOne(Product::class);
    }
}
