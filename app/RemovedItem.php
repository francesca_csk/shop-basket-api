<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RemovedItem extends Model
{
    // specify columns to mass assign (when using create method)
    protected $fillable = [ 'product_id', 'product_name', 'product_price', 'basket_id', 'user_id'];

    // timestamp false - using special time() call as needed
    public $timestamps = false;

}
