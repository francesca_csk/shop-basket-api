<?php

namespace App\Providers;

use Laravel\Passport\Passport; // for passport configuration
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // uncommented line below for boot method below
         'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // passport method to register routes to issue access tokens
        Passport::routes();

        // for testing purposes of defining personal access client manually
        //Passport::personalAccessClientId('client-id');

        // Used for testing purposes, to configure shorter token lifetimes
        // Passport::tokensExpireIn(now()->addDays(15));
        // Passport::refreshTokensExpireIn(now()->addDays(30));
    }
}
