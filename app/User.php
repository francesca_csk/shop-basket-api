<?php

namespace App;

use Laravel\Passport\HasApiTokens; // for passport configuration
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    // helper methods to inspect auth user's token & scopes
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

     /**
     * Find the user instance for the given username.
     *
     *Customizing username 
     *When authenticating using the password grant, 
     *Passport will use the email attribute of your model as the "username". 
     *However, you may customize this behavior 
     *by defining a findForPassport method on your model:
     * @param  string  $username
     * @return \App\User
     */
     /*public function findForPassport($username)
     {
         return $this->where('username', $username)->first();
     }*/


     /**
    * Validate the password of the user for the Passport password grant.
    *When authenticating using the password grant, 
    *Passport will use the password attribute of your model to validate 
    *the given password. If your model does not have a password attribute or you wish to customize the password validation logic, you can define a validateForPassportPasswordGrant method on your model:
    
    * @param  string $password
    * @return bool
    */
   /* public function validateForPassportPasswordGrant($password)
    {
        return Hash::check($password, $this->password);
    }*/
}
