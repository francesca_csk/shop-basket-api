<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    // Order:model $fillable field for permitted assignments to orders tbl
    protected $fillable = ['products','first_name','last_name', 'street_1','city','state', 'zip','country', 'email','phone', 'items_total', 'transaction_id'];
}
