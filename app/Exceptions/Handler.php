<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // modify ModelNotFoundException w/ custom error msg 
        if ($exception instanceof ModelNotFoundException && $request->wantsJson()) 
        {
            return response()->json([
              'error' => 'Resource not found.'
            ], 404);
        }

        // modify AuthenticationException w/ custom error msg
        if($exception instanceof \Illuminate\Auth\AuthenticationException )
        {
            return response()->json([
                'error' => 'Unauthorized. Make sure you have authenticated yourself and provided bearer token for authorization.'
              ], 401);
        }

        return parent::render($request, $exception);
    }

    
}
