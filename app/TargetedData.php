<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TargetedData extends Model
{
    // TargetedData Model - $fillable field for permitted assignments to targeted_data tbl
     protected $fillable = [ 'product_id', 'product_name', 'product_price', 'user_id', 'removed_at'];

    // timestamp false because removed_at timestamp is being retrieved from official time removed by remvoed_items tbl
     public $timestamps = false;
}
