# Shopping Basket API

As part of an online shopping platform, this project focused on serving as part of a shopping basket that provided the sales team with data that consisted of knowing which items were added to a basket, but removed before checkout.

The shopping basket is built as a RESTful API, using PHP with the laravel framework.  

## Minimal Requirements

-	Building a shopping basket that collects data for sales team.
-	The above mentioned data to collect are items added to basket but removed before checkout.
-	Future use of collected data is for targeted discounts.
-	Serves as part of an ongoing shopping platform development.
-	Minimal requirements: Using PHP.
-	Scope: Focusing on API side, not on the interface.

## Assumptions

###### Authentication & Authorization
For security measures authentication and authorization have been implemented using the laravel passport package and OAuth2. For development purposes, I integrated a simpler grant, for consumer testing purposes which uses a personal access token. Once the User has registered or logged in, a token will be assigned. This token will then be used in the Authorization header in all HTTP requests. However, the only exempt request, that is assumed not to require authorization is for the HTTP request that provides the list of all the available products. This product list consists of the dummy product data inside products.json file.

###### Removed Basket Items before Checkout
The data collected is to be served for the sales team to targeted discounts. It collects the user data of removed basket items by the user before checkout. With this, it’s assumed that the desired data to collect is only the removed basket items by users who checked out. Furthermore, making our targeted data only the basket items removed by users who did check out, not accounting for the users who removed an item from basket but never attempted to check out (thus, meaning abandoned baskets). All the items recorded for the targeted data, include only the removed items from baskets that have been checked out. To collect additional data, including abandoned basket items or items removed from baskets by users who did not check out, please see RemovedItem model to retrieve removed_items table which holds this unofficial data. Note official data, for the targeted data of basket items removed before checkout (by users who did in fact check out), is processed by the TargetedData model and can be retrieved with the GET HTTP call to /api/targeted-data-list. 

###### Secure Payment Processing
As part of the handling the lifecycle of a shopping basket, the basket includes processing checkouts for orders. For check outs, it’s assumed that the basket will be using part of the shopping platforms secure payment process in place through payment gateways, through gateway aggregation. Once the user decides to checkout with their basket item, they will provide their order information such as: first name, last name, email, & billing address. Once this part of the order is submitted, it’s assumed that the shopping platform then routes the users to the secure payment method, where it then collects the data for the payment to be processed such as: gateway, method, first name, last name, credit card number, expiration data & ccv. 

###### Other Assumptions:
-	Authenticated Users can create more than one basket.
-	It is assumed that the price for each product already includes the tax amount.  
-	Basket item quantity consists of a minimum of one with a maximum of three.
-	Data for product listings comes from products.json (sample data provided) – thus no need for paginated data.

## Decisions

###### Framework and Database
Laravel is used as the framework, with MySQL. 

###### API Side and Data storage
The shopping basket is built as a RESTful API. The shopping basket and its data is in full compliance with the RESTfulness Guidelines and uses the best practices (one of which is avoiding the use of sessions, as well as use of cookies due the small amount of data cookies hold). The data for the basket is persisted in the database and also fully compliant with the RESTfulness Guidelines. The data does not concern the state of the client, while the architecture makes each request independent of any sort of state (furthermore which results to the fundamental REST aspect, being stateless). 

###### Basket initialization 
There are two ways to initialize a shopping basket, however only one way produces the most productive and best way to go about initialization. The other and less desirable (the lazy way), is to initialize and create the shopping basket, when one first adds a product into the basket. However, this introduces a small ambiguity when the add a product to shopping basket call returns a HTTP Status 201 to indicate, one or more new resources have been created versus a HTTP Status 200 (OK) for any subsequent calls indicating that there is already an existing basket, which is just adding more products to it. Furthermore, resulting in using the more eager and most logical approach of separating the create a new basket call from the add the item to the basket call. Once the user creates the basket, it provides the user with two special parameters, _key & _token. The _key parameter serves as the basket key which each basket is associated with and used for any basket related operations, while the token parameter serves as the basket id which is used in the URL path (ex. /api/baskets/{basketid}. 

###### Specification which define the endpoints for shopping basket operations:
-	Get list with available products
-	Register new user to get access token
-	Login existing user to retrieve access token
-	Create new shopping basket
-	Add new item to shopping basket or update existing item quantity 
-	Remove item from shopping basket
-	Get shopping basket contents 
-	Check out with items in shopping basket
-	Destroy shopping basket
-	Retrieves order history made by user
-	***Generates specified list of targeted data items which contains the collection of all items removed from basket before checkout.

## API Documentation:

###### PLEASE SEE MY COMPLETE API DOCUMENTATION AND ALL API ENDPOINTS FOR THIS TECHNICAL ASSIGNMENT ON MY POSTMAN API DOCS WITH LINK BELOW:

## [shoppingbasket-apidocs-by-fk-link](https://documenter.getpostman.com/view/8654974/SVmtxJvA)

## Installation
In the project folder, save the '.env.example' file as '.env'. Make sure to input choice of database with credentials.
Next install, migrate, seed and serve. Follow the following commands:

Install dependencies

```sh
	$ Composer install
```

Migrate database

```sh
	 $ php artisan migrate	
```

Install laravel/passport package (automatically generates client id/secret)

```sh
	$ php artisan passport:install
```

Seed sample data from products.json file

```sh
	$ php artisan db:seed	
```

Start server

```sh
	$ php artisan serve	
```


## License

Copyright (c) 2019 F. Knudsen

[MIT license](https://opensource.org/licenses/MIT)

