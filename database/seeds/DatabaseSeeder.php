<?php

use Illuminate\Database\Seeder;

use App\Product;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        // using products.json data for testing 
        $productsJson = File::get("database/data/products.json");

        $data = json_decode($productsJson, true);
        foreach ($data['product'] as $obj) {
            Product::create(array(
              'name' => $obj['name'], 'price' => $obj['price']
              ));
        }




    }
}
