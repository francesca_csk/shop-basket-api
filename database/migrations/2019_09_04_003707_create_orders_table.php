<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id'); // order id 
            $table->longText('products'); // list of products in order
            $table->decimal('items_total'); // total price for order
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            // billing-address info
            $table->string('first_name'); // user name for order
            $table->string('last_name');
            $table->string('street_1'); // user address for order
            $table->string('city'); // user address for order
            $table->string('state'); // user address for order
            $table->string('zip'); // user address for order
            $table->string('country'); // user address for order
            $table->string('email'); // user address for order
            $table->string('transaction_id');  // transaction id to record with each order
            $table->timestamps(); // timestamp for created order
            //order status - ex: "Awaiting Payment"
            //$table->string('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
