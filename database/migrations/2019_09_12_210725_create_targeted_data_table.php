<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTargetedDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('targeted_data', function (Blueprint $table) {
            $table->bigIncrements('id'); // as unique primary key
            $table->unsignedBigInteger('product_id'); // as foreign key reference 
            $table->string('product_name'); 
            $table->decimal('product_price');
            $table->timestamp('removed_at')->useCurrent();
            $table->unsignedInteger('user_id');
            $table->foreign('product_id')->references('id')->on('products'); // assign product_id as foreign key
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('targeted_data');
    }
}
